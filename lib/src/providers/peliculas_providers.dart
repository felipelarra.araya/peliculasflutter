import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:peliculas/src/models/pelicula.model.dart';

class PeliculasProvider{
  String _apiKey = 'd757bafc6994697871d3607e6acf8888';
  String _url    = 'api.themoviedb.org';
  String _language = 'es-ES'; 


Future<List<Pelicula>> _procesarRespuesta(Uri url) async{

  final resp = await http.get(url);
  final decodedData = json.decode(resp.body);
  final peliculas = new Peliculas.fromJsonList(decodedData['results']);


  return peliculas.items;
}

Future<List<Pelicula>> getEnCines() async{

  final url = Uri.https(_url, '3/movie/now_playing',{
    'api_key' : _apiKey,
    'language': _language
  });

  return await _procesarRespuesta(url);
}

Future<List<Pelicula>> getPopulares() async{

  final url = Uri.https(_url, '3/movie/popular',{
    'api_key' : _apiKey,
    'language': _language
  });

  return await _procesarRespuesta(url);
}



}